from datetime import datetime

from mock import patch
from mock import Mock
import pytz

from salsa import time


def test_utcnow():
    expected = datetime.now(pytz.utc)
    dt_now = Mock(now=lambda _: expected)
    with patch("salsa.time.datetime", dt_now):
        assert time.utcnow() == expected


def test_utcnow_timezone():
    assert time.utcnow().tzinfo == pytz.utc
