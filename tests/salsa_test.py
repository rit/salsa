from sqlalchemy import Table
from sqlalchemy.ext.declarative import DeferredReflection

from salsa import Base


# https://docs.sqlalchemy.org/en/14/orm/declarative_tables.html#using-deferredreflection
class Reflected(DeferredReflection):
    __abstract__ = True


class Game(Reflected, Base):
    __table__ = Table("games", Base.metadata)


def test_utc_timezone(morning, evening, dbsession, dbengine):
    Reflected.prepare(dbengine)

    game = Game(home="England", away="France", kickoff_at=morning)
    dbsession.add(game)
    dbsession.commit()

    # Fetch kickoff_at from database
    dbsession.refresh(game)
    assert game.kickoff_at == morning
