import os
import pytest
from sqlalchemy import create_engine

from salsa import testing


@pytest.fixture
def dbsession(request, dbengine):
    return testing.dbsession(request, dbengine)


@pytest.fixture
def morning():
    return testing.morning()


@pytest.fixture
def evening():
    return testing.evening()


@pytest.fixture
def py3k_now(request):
    return testing.py3k_now(request)


@pytest.fixture
def password(request):
    return testing.password(request)


@pytest.fixture
def dbengine():
    dburl = os.environ["SALSA_TEST_DBURL"]
    return create_engine(dburl)
