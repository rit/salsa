from datetime import datetime


from salsa import time


def test_dbsession(dbsession):
    q = dbsession.execute("select 1").fetchone()
    assert q[0] == 1


def test_morning(morning):
    now = datetime.now()
    assert morning.day == now.day
    assert morning.hour == 5


def test_evening(evening):
    now = datetime.now()
    assert evening.day == now.day
    assert evening.hour == 19


def test_py3k_now(py3k_now):
    now = time.utcnow()
    assert now.year == 2008


def test_password(password):
    assert password.plaintext == "dragon"
