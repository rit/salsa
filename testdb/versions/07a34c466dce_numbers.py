"""numbers

Revision ID: 07a34c466dce
Revises: 5cbce37a4544
Create Date: 2016-08-30 17:58:39.605587

"""

# revision identifiers, used by Alembic.
revision = "07a34c466dce"
down_revision = "5cbce37a4544"
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table("numbers", sa.Column("number", sa.Integer))


def downgrade():
    op.drop_table("games")
