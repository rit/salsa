"""Create games table

Revision ID: 5cbce37a4544
Revises: 
Create Date: 2016-06-16 21:27:22.072966

"""

# revision identifiers, used by Alembic.
revision = "5cbce37a4544"
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "games",
        sa.Column("uid", sa.Integer, primary_key=True),
        sa.Column("home", sa.String, nullable=False),
        sa.Column("away", sa.Unicode, nullable=False),
        sa.Column("kickoff_at", sa.DateTime(), nullable=False),
    )


def downgrade():
    op.drop_table("games")
