clean:
	@@find . -name __pycache__ | xargs rm -rf
	@@find . -name '*.pyc' | xargs rm

gentags:
	ctags --extra=+f -R .

coverage:
	poetry run pytest --cov-config .coveragerc --cov=salsa tests

style:
	@@pycodestyle --max-line-length 100 --ignore=E731 salsa/*.py tests/*.py

autoflake:
	poetry run autoflake -i -r --remove-all-unused-imports salsa/*.py tests/*.py

sdist:
	rm -rf salsa.egg-info dist && python setup.py sdist

req:
	diff requirements* | sed -n 's/^> \(.*\)/\1/p' > req.txt
